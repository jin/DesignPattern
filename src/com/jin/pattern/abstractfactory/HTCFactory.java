package com.jin.pattern.abstractfactory;

import com.jin.pattern.factorymethod.HTC;
import com.jin.pattern.factorymethod.Mobile;

public class HTCFactory implements IFactory {

	@Override
	public Mobile produce() {
		return new HTC();
	}

}
