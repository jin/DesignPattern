package com.jin.pattern.abstractfactory;

import com.jin.pattern.factorymethod.Mobile;

/**
 * 抽象工厂模式(Abstract Factory)
 * 对于工厂方法模式，如果增加一种生产的种类，不得不修改工厂类，
 * 从设计模式看，违背了开闭原则，而抽象工厂模式很好的解决了这个问题，
 * 只需实现IFactory新建一个类，不用改动原有代码，所以扩展性较好
 * @author Jin
 * @datetime 2015/1/10 下午2:08:11
 */
interface IFactory {
	
	public Mobile produce();
	
}
