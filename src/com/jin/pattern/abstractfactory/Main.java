package com.jin.pattern.abstractfactory;

import com.jin.pattern.factorymethod.Mobile;

public class Main {

	public static void main(String[] args) {
		IFactory xiaoMiFac = new XiaoMiFactory();
		Mobile xiaoMi = xiaoMiFac.produce();
		xiaoMi.photograph();
		
		IFactory htcFac = new HTCFactory();
		Mobile htc = htcFac.produce();
		htc.photograph();
	}
}
