package com.jin.pattern.abstractfactory;

import com.jin.pattern.factorymethod.Mobile;
import com.jin.pattern.factorymethod.XiaoMi;

public class XiaoMiFactory implements IFactory {

	@Override
	public Mobile produce() {
		return new XiaoMi();
	}

}
