package com.jin.pattern.adapter;

/**
 * 已存在的、具有特殊功能、但不符合我们既有标准接口的类
 * @author Jin
 * @datetime 2015/1/20 上午10:49:42
 */
public class ClassAdaptee {

	public void method(){
		System.out.println("ClassAdaptee:Do something！");
	}
}
