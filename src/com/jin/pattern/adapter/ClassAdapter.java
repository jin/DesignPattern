package com.jin.pattern.adapter;

/**
 * 类的适配器模式
 * @author Jin
 * @datetime 2015/1/20 上午10:50:28
 */
public class ClassAdapter extends ClassAdaptee implements Target {

	@Override
	public void method1() {
		super.method();
	}

	@Override
	public void method2() {
		System.out.println("ClassAdapter:method2");
	}

}
