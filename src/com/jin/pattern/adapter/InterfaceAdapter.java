package com.jin.pattern.adapter;

/**
 * 接口的适配器模式
 * @author Jin
 * @datetime 2015/1/20 上午10:53:59
 */
public abstract class InterfaceAdapter implements Target {

	@Override
	public void method1() {
		System.out.println("InterfaceAdapter:method1 do nothing,please override in subclass!");
	}

	@Override
	public void method2() {
		System.out.println("InterfaceAdapter:method2 do nothing,please override in subclass!");
	}

}
