package com.jin.pattern.adapter;

/**
 * 调用类，目标接口Target
 * @author Jin
 * @datetime 2015/1/20 上午11:03:42
 */
public class Main {

	public static void main(String[] args) {
		
		Target objectAdapter = new ObjectAdapter(new ObjectAdaptee());
		objectAdapter.method1();
		objectAdapter.method2();
		
		Target classAdapter = new ClassAdapter();
		classAdapter.method1();
		classAdapter.method2();
		
		Target interfaceAdapter = new SubClass();
		interfaceAdapter.method1();
		interfaceAdapter.method2();
		
	}
	
}
