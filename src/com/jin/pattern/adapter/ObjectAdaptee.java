package com.jin.pattern.adapter;

/**
 * 已存在的、具有特殊功能、但不符合我们既有标准接口的类
 * @author Jin
 * @datetime 2015/1/20 上午10:46:48
 */
public class ObjectAdaptee {

	public void method(){
		System.out.println("ObjectAdaptee:Do something！");
	}
}
