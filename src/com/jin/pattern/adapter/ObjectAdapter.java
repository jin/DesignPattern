package com.jin.pattern.adapter;

/**
 * 对象的适配器模式
 * @author Jin
 * @datetime 2015/1/20 上午10:43:55
 */
public class ObjectAdapter implements Target {

	private ObjectAdaptee adaptee;
	
	public ObjectAdapter(ObjectAdaptee adaptee) {
		this.adaptee = adaptee;
	}

	@Override
	public void method1() {
		adaptee.method();
	}

	@Override
	public void method2() {
		System.out.println("ObjectAdapter:method2");
	}

}
