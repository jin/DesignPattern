package com.jin.pattern.adapter;

/**
 * 继承抽象适配类，仅实现需要的方法
 * @author Jin
 * @datetime 2015/1/20 上午11:00:31
 */
public class SubClass extends InterfaceAdapter {

	@Override
	public void method2() {
		System.out.println("SubClass:method2 do something");
	}

}
