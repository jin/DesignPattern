package com.jin.pattern.adapter;

/**
 * 目标接口
 * @author Jin
 * @datetime 2015/1/20 上午10:39:26
 */
public interface Target {

	public void method1();
	
	public void method2();
	
}
