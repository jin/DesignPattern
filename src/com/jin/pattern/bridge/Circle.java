package com.jin.pattern.bridge;

public class Circle extends Shape {

	private int radius;

	public Circle(Draw draw, int radius) {
		super(draw);
		this.radius = radius;
	}

	
	@Override
	public void draw() {
		getDraw().drawCircle(radius);
	}

	public int getRadius() {
		return radius;
	}

	public void setRadius(int radius) {
		this.radius = radius;
	}

}
