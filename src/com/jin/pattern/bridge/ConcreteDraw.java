package com.jin.pattern.bridge;

public class ConcreteDraw {

	public static final boolean isWindows = System.getProperty("os.name").toLowerCase().indexOf("windows") != -1;
	public static final boolean isLinux = System.getProperty("os.name").toLowerCase().indexOf("linux") != -1;
	
	public static Draw get() {
		Draw draw = null;
		if(isWindows){
			draw = new WindowsDraw();
		}else if(isLinux){
			draw = new LinuxDraw();
		}else{
			System.out.println("Your operating system not support!");
		}
		return draw;
	}

}
