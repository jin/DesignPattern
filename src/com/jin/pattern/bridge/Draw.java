package com.jin.pattern.bridge;

public interface Draw {

	public void drawRectangle(int width, int height);
	public void drawCircle(int radius);
	
}
