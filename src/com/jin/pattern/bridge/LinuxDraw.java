package com.jin.pattern.bridge;

public class LinuxDraw implements Draw {

	public void drawRectangle(int width, int height){
		System.out.println("Linux:Draw a rectangle.");
	}
	
	public void drawCircle(int radius){
		System.out.println("Linux:Draw a circle.");
	}
	
}
