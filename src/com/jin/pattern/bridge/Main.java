package com.jin.pattern.bridge;

public class Main {

	public static void main(String[] args) {
		Rectangle rectangle = new Rectangle(ConcreteDraw.get(), 20, 30);
		Circle circle = new Circle(ConcreteDraw.get(), 50);
		rectangle.draw();
		circle.draw();
	}
	
}
