package com.jin.pattern.bridge;

public class Rectangle extends Shape {

	private int width;
	private int height;
	
	public Rectangle(Draw draw, int width, int height) {
		super(draw);
		this.width = width;
		this.height = height;
	}

	@Override
	public void draw() {
		getDraw().drawRectangle(width, height);

	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

}
