package com.jin.pattern.bridge;

/**
 * 桥接模式
 * 将抽象部分与实现部分分离，使它们都可以独立的变化
 * 不要潜意识中将接口与其实现类套用到这里来理解这句话！
 * 桥接模式指将提供对外的抽象接口与内部的具体实现相分离，例如本例，我现在想开发一个跨平台的图形界面绘画库，
 * 目前支持Windows和Linux平台，如果我想使图形库支持MAC OS，实现Draw接口并略微修改ConcreteDraw即可，
 * 即便某天我想提供画三角形的功能，只需要在Draw中给出功能定义并在其实现类中给出功能实现，而客户端升级绘画库后，
 * 对原有功能无任何变动及影响，但无形中增加了客户端应用的跨平台性及更丰富的功能。
 * @author Jin
 * @datetime 2015/1/22 下午1:36:05
 */
public abstract class Shape {

	private Draw draw;

	public Shape(Draw draw) {
		this.draw = draw;
	}
	
	public abstract void draw();

	public Draw getDraw() {
		return draw;
	}
	
}
