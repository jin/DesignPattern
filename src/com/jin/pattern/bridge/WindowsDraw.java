package com.jin.pattern.bridge;

public class WindowsDraw implements Draw {

	public void drawRectangle(int width, int height){
		System.out.println("Windows:Draw a rectangle.");
	}
	
	public void drawCircle(int radius){
		System.out.println("Windows:Draw a circle.");
	}
}
