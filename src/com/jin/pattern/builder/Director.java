package com.jin.pattern.builder;

/**
 * 导演类,定义建造者的建造过程
 * @author Jin
 * @datetime 2015/1/12 下午6:25:13
 */
public class Director {

	public static void direct(IBuilder builder) {
		builder.builderScreen();
		builder.builderRom();
	}

}
