package com.jin.pattern.builder;

/**
 * 建造者
 * @author Jin
 * @datetime 2015/1/12 下午6:24:35
 */
public class HTCBuilder implements IBuilder {
	
	private Mobile mobile = new Mobile("HTC");

	@Override
	public void builderScreen() {
		mobile.setScreen("5.0寸");
	}

	@Override
	public void builderRom() {
		mobile.setRom("2G");
	}

	@Override
	public Mobile getResult() {
		return mobile;
	}

}
