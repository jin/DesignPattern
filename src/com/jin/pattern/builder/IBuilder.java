package com.jin.pattern.builder;

/**
 * 抽象建造者，定义建造者的建造项
 * @author Jin
 * @datetime 2015/1/12 下午6:16:27
 */
interface IBuilder {

	public void builderScreen();
	public void builderRom();
	
	public Mobile getResult();
	
	
}
