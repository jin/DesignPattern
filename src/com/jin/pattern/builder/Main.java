package com.jin.pattern.builder;

public class Main {

	public static void main(String[] args) {
		XiaoMiBuilder xiaoMiBuilder = new XiaoMiBuilder();
		HTCBuilder htcBuilder = new HTCBuilder();
		Director.direct(xiaoMiBuilder);
		Director.direct(htcBuilder);
		Mobile xiaoMi = xiaoMiBuilder.getResult();
		Mobile htc = htcBuilder.getResult();
		System.out.println(xiaoMi.toString());
		System.out.println(htc.toString());
	}
	
}
