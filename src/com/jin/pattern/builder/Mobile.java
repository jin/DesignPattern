package com.jin.pattern.builder;

/**
 * 产品类
 * @author Jin
 * @datetime 2015/1/12 下午5:59:47
 */
public class Mobile {

	private String name;
	private String screen;
	private String rom;
	public Mobile(String name) {
		super();
		this.name = name;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getScreen() {
		return screen;
	}
	public void setScreen(String screen) {
		this.screen = screen;
	}
	public String getRom() {
		return rom;
	}
	public void setRom(String rom) {
		this.rom = rom;
	}
	@Override
	public String toString() {
		return "Mobile [name=" + name + ", screen=" + screen + ", rom=" + rom
				+ "]";
	}
	
}
