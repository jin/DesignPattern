package com.jin.pattern.builder;

/**
 * 建造者
 * @author Jin
 * @datetime 2015/1/12 下午6:16:44
 */
public class XiaoMiBuilder implements IBuilder {

	Mobile mobile = new Mobile("XiaoMi");
	
	@Override
	public void builderScreen() {
		mobile.setScreen("4.7寸");
	}

	@Override
	public void builderRom() {
		mobile.setRom("3G");
	}

	@Override
	public Mobile getResult() {
		return mobile;
	}

}
