package com.jin.pattern.chainofresponsibility;

/**
 * 责任链模式中，有很多对象，每一个对象持有下一个对象的引用，这样就形成了一条链，请求在这条链上传递处理，
 * 发出这个请求的客户端并不知道链上的哪一个对象最终处理这个请求，这使得系统可以在不影响客户端的情况下动态地重新组织和分配责任。
 * @author Jin
 * @datetime 2015/1/28 上午10:09:15
 */
public abstract class Handler {

	private Handler handler;

	public Handler getHandler() {
		return handler;
	}

	public void setHandler(Handler handler) {
		this.handler = handler;
	}
	
	public abstract String handleRequest(String request);
}
