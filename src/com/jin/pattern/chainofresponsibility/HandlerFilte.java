package com.jin.pattern.chainofresponsibility;

/**
 * 返回参数字符串过滤掉字符'a'和'A'的新字符串
 * @author Jin
 * @datetime 2015/1/28 上午9:42:51
 */
public class HandlerFilte extends Handler {

	@Override
	public String handleRequest(String request) {
		if(getHandler() != null){
			request = getHandler().handleRequest(request);
		}
		char[] chars = request.toCharArray();
		request = "";
		for (char c : chars) {
			if(c != 65 && c != 97){
				request += c;
			}
		}
		return request;
	}

}
