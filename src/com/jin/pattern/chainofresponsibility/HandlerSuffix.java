package com.jin.pattern.chainofresponsibility;

/**
 * 返回参数字符串添加系统指定后缀的新字符串
 * @author Jin
 * @datetime 2015/1/28 上午9:38:58
 */
public class HandlerSuffix extends Handler {

	public static final String SUFFIX = ".suffix";
	
	@Override
	public String handleRequest(String request) {
		if(getHandler() != null){
			request = getHandler().handleRequest(request);
		}
		return request + SUFFIX;
	}

}
