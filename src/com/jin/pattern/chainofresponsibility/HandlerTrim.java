package com.jin.pattern.chainofresponsibility;

/**
 * 返回参数字符串去除首尾空格后的新字符串
 * @author Jin
 * @datetime 2015/1/28 上午9:35:16
 */
public class HandlerTrim extends Handler {

	@Override
	public String handleRequest(String request) {
		if(getHandler() != null){
			request = getHandler().handleRequest(request);
		}
		return request.trim();
	}

}
