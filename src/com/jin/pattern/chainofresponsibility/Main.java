package com.jin.pattern.chainofresponsibility;

public class Main {

	public static void main(String[] args) {
		Handler handlerSuffix = new HandlerSuffix();
		Handler handlerFilte = new HandlerFilte();
		Handler handlerTrim = new HandlerTrim();
		handlerSuffix.setHandler(handlerFilte);
		handlerFilte.setHandler(handlerTrim);
		
		String request = "  I am a Java programer  ";
		String newRequest = handlerSuffix.handleRequest(request);
		System.out.println(request);
		System.out.println(newRequest);
	}
}
