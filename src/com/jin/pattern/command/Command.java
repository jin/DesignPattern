package com.jin.pattern.command;

/**
 * 抽象命令接口
 * @author Jin
 * @datetime 2015/1/30 下午4:57:25
 */
public interface Command {

	public void execute();
	
}
