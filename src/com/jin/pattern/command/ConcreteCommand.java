package com.jin.pattern.command;

/**
 * 实际命令对象
 * @author Jin
 * @datetime 2015/1/30 下午4:57:58
 */
public class ConcreteCommand implements Command {

	private Receiver receiver;
	
	public ConcreteCommand(Receiver receiver) {
		this.receiver = receiver;
	}

	@Override
	public void execute() {
		receiver.doSomething();
	}

}
