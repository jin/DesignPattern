package com.jin.pattern.command;

/**
 * 请求者
 * @author Jin
 * @datetime 2015/1/30 下午5:00:48
 */
public class Invoker {

	private Command command;

	public Invoker(Command command) {
		this.command = command;
	}
	
	public void doSomething(){
		command.execute();
	}
	
}
