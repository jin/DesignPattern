package com.jin.pattern.command;

/**
 * 命令模式
 * 实现命令的发出者与命令具体执行者解耦
 * @author Jin
 * @datetime 2015/1/30 下午5:01:53
 */
public class Main {

	public static void main(String[] args) {
		Receiver receiver = new Receiver();
		Command command = new ConcreteCommand(receiver);
		Invoker invoker = new Invoker(command);
		invoker.doSomething();
	}
}
