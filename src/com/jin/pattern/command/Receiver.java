package com.jin.pattern.command;

/**
 * 执行者
 * @author Jin
 * @datetime 2015/1/30 下午4:51:45
 */
public class Receiver {

	public void doSomething(){
		System.out.println("Do something!");
	}
}
