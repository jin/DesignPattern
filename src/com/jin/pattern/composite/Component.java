package com.jin.pattern.composite;

import java.util.Enumeration;

public interface Component {

	public void add(Component component);
	public void remove(Component component);
	public Enumeration<Component> getChildren();
	public String getName();
}
