package com.jin.pattern.composite;

import java.util.Enumeration;
import java.util.Vector;

/**
 * 组合模式
 * 将对象组合成树形结构来表现“整体/部分”的层次结构，能让客户端已一致的方式处理个别对象和组合对象
 * @author Jin
 * @datetime 2015/1/22 下午3:34:31
 */
public class Composite implements Component {

	private String name;
	private Vector<Component>components = new Vector<>();
	
	public Composite(String name) {
		this.name = name;
	}

	@Override
	public void add(Component component) {
		components.add(component);
	}

	@Override
	public void remove(Component component) {
		components.remove(component);
	}

	@Override
	public Enumeration<Component> getChildren() {
		return components.elements();
	}

	@Override
	public String getName() {
		return this.name;
	}

}
