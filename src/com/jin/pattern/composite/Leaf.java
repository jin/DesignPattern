package com.jin.pattern.composite;

import java.util.Enumeration;

public class Leaf implements Component {

	private String name;
	
	public Leaf(String name) {
		this.name = name;
	}

	@Override
	public void add(Component component) {
		throw new RuntimeException();
	}

	@Override
	public void remove(Component component) {
		throw new RuntimeException();
	}

	@Override
	public Enumeration<Component> getChildren() {
		throw new RuntimeException();
	}

	@Override
	public String getName() {
		return this.name;
	}

	

}
