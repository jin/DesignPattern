package com.jin.pattern.composite;

import java.util.Enumeration;

public class Main {

	public static void main(String[] args) {
		Component root = new Composite("根节点");
		Component node = new Composite("子节点");
		Component leaf1 = new Leaf("叶子1");
		Component leaf2 = new Leaf("叶子2");
		Component leaf3 = new Leaf("叶子3");
		root.add(node);
		root.add(leaf1);
		node.add(leaf2);
		node.add(leaf3);
		
		Enumeration<Component>ennum1 = root.getChildren();
		while (ennum1.hasMoreElements()) {
			Component component = ennum1.nextElement();
			System.out.println(component.getName());
		}
		
	}
}
