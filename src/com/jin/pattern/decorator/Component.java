package com.jin.pattern.decorator;

public interface Component {

	public void operation();
	
}
