package com.jin.pattern.decorator;

public class ConcreteComponent implements Component {

	@Override
	public void operation() {
		System.out.println("Do something!");
	}

}
