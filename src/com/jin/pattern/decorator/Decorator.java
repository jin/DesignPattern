package com.jin.pattern.decorator;

/**
 * 装饰器模式
 * 装饰对象和被装饰对象实现同一个接口，且装饰对象持有被装饰对象的实例
 * 典型应用如Java中的IO流体系
 * BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream("pathname")))
 * @author Jin
 * @datetime 2015/1/20 下午5:06:43
 */
public class Decorator implements Component {

	private Component component;
	
	public Decorator(Component component) {
		this.component = component;
	}

	@Override
	public void operation() {
		System.out.println("Do before!");
		component.operation();
		System.out.println("Do after!");
	}

}
