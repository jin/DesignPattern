package com.jin.pattern.decorator;

public class Main {

	public static void main(String[] args) {
		Component component = new Decorator(new ConcreteComponent());
		component.operation();
	}
}
