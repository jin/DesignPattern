package com.jin.pattern.facade;

/**
 * 银行存款系统
 * @author Jin
 * @datetime 2015/1/21 下午5:59:15
 */
public class BankSystem {

	public boolean verification(Person p){
		System.out.println("The man has enough deposit！");
		return true;
	}
}
