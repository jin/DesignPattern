package com.jin.pattern.facade;

/**
 * 征信系统
 * @author Jin
 * @datetime 2015/1/21 下午5:52:24
 */
public class CreditSystem {
	
	public boolean verification(Person p){
		System.out.println("The man have good credit records!");
		return true;
	}
}
