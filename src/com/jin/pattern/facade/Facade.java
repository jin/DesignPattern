package com.jin.pattern.facade;

/**
 * 外观模式
 * 屏蔽子系统间的复杂操作，将子系统与客户程序解耦
 * 示例为验证借款人在公安系统、征信系统、银行系统中的记录是否良好，作为借贷机构是否贷款的参考信息
 * @author Jin
 * @datetime 2015/1/21 下午6:19:17
 */
public class Facade {

	private PoliceSystem policeSystem;
	private CreditSystem creditSystem;
	private BankSystem bankSystem;
	
	public Facade() {
		this.policeSystem = new PoliceSystem();
		this.creditSystem = new CreditSystem();
		this.bankSystem = new BankSystem();
	}
	
	public void verification(Person p){
		if(policeSystem.verification(p) && creditSystem.verification(p) && bankSystem.verification(p)){
			System.out.println("This man meet the requirements,you can loan money to him.");
		}else{
			System.out.println("Not recommended loan money to him.");
		}
	}
	
	
}
