package com.jin.pattern.facade;

public class Main {

	public static void main(String[] args) {
		Person p = new Person();
		Facade facade = new Facade();
		facade.verification(p);
	}
}
