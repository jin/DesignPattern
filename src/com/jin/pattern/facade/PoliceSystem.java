package com.jin.pattern.facade;

/**
 * 公安部系统
 * @author Jin
 * @datetime 2015/1/21 下午5:56:16
 */
public class PoliceSystem {

	public boolean verification(Person p){
		System.out.println("The man have no criminal records!");
		return true;
	}
}
