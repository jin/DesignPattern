package com.jin.pattern.factorymethod;

public class HTC implements Mobile {

	@Override
	public void photograph() {
		System.out.println("take a picture by XiaoMi.");
	}

}
