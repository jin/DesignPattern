package com.jin.pattern.factorymethod;

public class Main {

	public static void main(String[] args) {
		Mobile xiaoMi = MobileFactory.produceXiaoMi();
		Mobile htc = MobileFactory.produceHTC();
		xiaoMi.photograph();
		htc.photograph();
	}
	
}
