package com.jin.pattern.factorymethod;

public interface Mobile {
	
	public void photograph();

}
