package com.jin.pattern.factorymethod;

/**
 * 工厂方法模式（Factory Method）
 * @author Jin
 * @datetime 2015/1/10 下午1:52:47
 */
public class MobileFactory {

	public static Mobile produceHTC(){
		return new HTC();
	}
	public static Mobile produceXiaoMi(){
		return new XiaoMi();
	}
	
}

/**
 * 普通工厂模式
 * 根据传入的参数自行判断产生的实例对象，虽简化了暴露的接口，但是耦合度大，使用较少
 * @author Jin
 * @datetime 2015/1/10 下午1:53:22
 */
class MobileFactory1{
	public static Mobile produce(String type){
		if("XiaoMi".equals(type)){
			return new XiaoMi();
		}else if("HTC".equals(type)){
			return new HTC();
		}else{
			throw new RuntimeException("type is wrong!");
		}
	}
}