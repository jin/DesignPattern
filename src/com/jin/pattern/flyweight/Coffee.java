package com.jin.pattern.flyweight;


/**
 * 具体享元角色（咖啡）
 * @author Jin
 * @datetime 2015/1/23 上午11:22:20
 */
public class Coffee extends Drink {

	public Coffee(String name) {
		super(name);
		setCategory("Coffee");
	}

	@Override
	public void info() {
		System.out.println("Coffee:["+getName()+"]");
	}

}
