package com.jin.pattern.flyweight;

/**
 * 抽象享元角色（饮品）
 * @author Jin
 * @datetime 2015/1/23 上午10:40:25
 */
public abstract class Drink {

	/**
	 * intrinsic state（内蕴状态）
	 */
	private String category;
	/**
	 * extrinsic state（外蕴状态）
	 */
	private String name;
	
	public Drink(String name) {
		this.name = name;
	}

	public abstract void info();

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
}
