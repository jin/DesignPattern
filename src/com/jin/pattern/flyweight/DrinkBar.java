package com.jin.pattern.flyweight;

import java.util.ArrayList;
import java.util.List;

/**
 * 享元工厂角色
 * @author Jin
 * @datetime 2015/1/23 上午11:25:09
 */
public class DrinkBar {

	private static DrinkBar bar = new DrinkBar();
	
	private static List<Drink>drinks = new ArrayList<>();
	
	private DrinkBar(){};
	
	public static DrinkBar getInstance(){
		return bar;
	}
	
	public Drink produce(String name, Category category){
		Drink drink = null;
		for (Drink d : drinks) {
			if(name.equals(d.getName())){
				drink = d;
				break;
			}
		}
		if(drink == null){
			if(category == Category.C){
				drink = new Coffee(name);
			}else if(category == Category.M){
				drink = new MilkTea(name);
			}else{
				throw new RuntimeException();
			}
		}
		drink.info();
		drinks.add(drink);
		return drink;
	}
	
	public void getAllInfo(){
		String cStr = "Coffee:[";
		String mStr = "Mile tea:[";
		for (Drink drink : drinks) {
			if(drink instanceof Coffee){
				cStr += drink.getName() + ","; 
			}else if(drink instanceof MilkTea){
				mStr += drink.getName() + ","; 
			}
		}
		System.out.println("卖出的所有口味：");
		System.out.println(cStr.substring(0, cStr.length()-1)+"]");
		System.out.println(mStr.substring(0, mStr.length()-1)+"]");
	}
	
}

enum Category{
	C,M
}
