package com.jin.pattern.flyweight;

/**
 * 享元模式
 * 采用一个共享来避免大量拥有相同内容对象的开销
 * 以共享的方式高效的支持大量的细粒度对象
 * @author Jin
 * @datetime 2015/1/23 下午2:39:15
 */
public class Main {

	public static void main(String[] args) {
		DrinkBar bar = DrinkBar.getInstance();
		bar.produce("卡布奇诺", Category.C);
		bar.produce("原味奶茶", Category.M);
		bar.produce("香草星冰乐", Category.C);
		bar.produce("黑糖烧仙草", Category.M);
		bar.produce("原味奶茶", Category.M);
		bar.produce("拿铁", Category.C);
		bar.produce("香草星冰乐", Category.C);
		bar.produce("拿铁", Category.C);
		bar.produce("卡布奇诺", Category.C);
		bar.produce("摩卡", Category.C);
		bar.produce("英伯伦红茶", Category.M);
		bar.getAllInfo();
	}
	
}
