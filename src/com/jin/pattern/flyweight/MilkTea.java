package com.jin.pattern.flyweight;

/**
 * 具体享元角色（奶茶）
 * @author Jin
 * @datetime 2015/1/23 上午11:19:51
 */
public class MilkTea extends Drink {

	public MilkTea(String name) {
		super(name);
		setCategory("Milk tea");
	}

	@Override
	public void info() {
		System.out.println("Milk tea:["+getName()+"]");
	}

}
