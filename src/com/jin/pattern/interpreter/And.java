package com.jin.pattern.interpreter;

/**
 * 非终结符表达式 
 * “与”操作
 * @author Jin
 * @datetime 2015年1月31日 下午6:13:16
 */
public class And extends Expression {

	private Expression left, right;

	public And(Expression left, Expression right) {
		this.left = left;
		this.right = right;
	}

	@Override
	public boolean interpret(Context ctx) {
		return left.interpret(ctx) && right.interpret(ctx);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj != null && obj instanceof And) {
			return left.equals(((And) obj).left)
					&& right.equals(((And) obj).right);
		}
		return false;
	}

	@Override
	public int hashCode() {
		return this.toString().hashCode();
	}

	@Override
	public String toString() {
		return "(" + left.toString() + " AND " + right.toString() + ")";
	}

}
