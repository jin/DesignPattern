package com.jin.pattern.interpreter;

/**
 * 终结符表达式
 * 布尔常量
 * @author Jin
 * @datetime 2015年1月31日 下午6:09:13
 */
public class Constant extends Expression {

	private boolean value;
	
	public Constant(boolean value) {
		this.value = value;
	}

	@Override
	public boolean interpret(Context ctx) {
		return value;
	}

	@Override
	public boolean equals(Object obj) {
		if(obj != null && obj instanceof Constant){
            return this.value == ((Constant)obj).value;
        }
        return false;
	}

	@Override
	public int hashCode() {
		return this.toString().hashCode();
	}

	@Override
	public String toString() {
		return new Boolean(value).toString();
	}

}
