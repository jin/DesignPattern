package com.jin.pattern.interpreter;

import java.util.HashMap;
import java.util.Map;

/**
 * 环境角色
 * @author Jin
 * @datetime 2015年1月31日 下午5:39:07
 */
public class Context {

	private Map<Variable, Boolean> map = new HashMap<Variable, Boolean>();
	
	public void assign(Variable var,boolean value){
		map.put(var, new Boolean(value));
	}
	
	public boolean lookup(Variable var){
		Boolean value = map.get(var);
		if(value == null){
			throw new IllegalArgumentException();
		}
		return value.booleanValue();
	}
}
