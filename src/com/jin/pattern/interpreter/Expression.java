package com.jin.pattern.interpreter;

/**
 * 抽象表达式角色
 * @author Jin
 * @datetime 2015年1月31日 下午5:36:47
 */
public abstract class Expression {

	public abstract boolean interpret(Context ctx);
	public abstract boolean equals(Object obj);
	public abstract int hashCode();
	public abstract String toString();
}
