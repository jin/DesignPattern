package com.jin.pattern.interpreter;

/**
 * 非终结符表达式 
 * “非”操作
 * @author Jin
 * @datetime 2015年1月31日 下午6:28:24
 */
public class Not extends Expression {

	private Expression exp;

	public Not(Expression exp) {
		this.exp = exp;
	}

	@Override
	public boolean interpret(Context ctx) {
		return !exp.interpret(ctx);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj != null && obj instanceof Not) {
			return exp.equals(((Not) obj).exp);
		}
		return false;
	}

	@Override
	public int hashCode() {
		return this.toString().hashCode();
	}

	@Override
	public String toString() {
		return "(Not " + exp.toString() + ")";
	}

}
