package com.jin.pattern.iterator;

interface Collection<T> {
	
	/**
	 * 返回列表中指定位置的元素
	 * @param index
	 * @return
	 */
	public T get(int index);
	
	/**
	 * 返回列表中的元素数
	 * @return
	 */
	public int size();
	
	/**
	 * 返回在列表的元素上进行迭代的迭代器
	 * @return
	 */
	public Iterator<T> iterator();

}
