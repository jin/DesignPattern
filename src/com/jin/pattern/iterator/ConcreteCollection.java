package com.jin.pattern.iterator;

public class ConcreteCollection<T> implements Collection<T> {
	
	private T[] array = null;

	public ConcreteCollection(T[] array) {
		this.array = array;
	}

	@Override
	public T get(int index) {
		return array[index];
	}

	@Override
	public int size() {
		return array.length;
	}

	@Override
	public Iterator<T> iterator() {
		return new ConcreteIterator();
	}
	
	private class ConcreteIterator implements Iterator<T>{
		
		private int index;
		

		public ConcreteIterator() {
			this.index = 0;
		}

		@Override
		public boolean hasNext() {
			return index < size();
		}

		@Override
		public T next() {
			if(index < size()){
				return array[index++];
			}
			throw new RuntimeException();
		}

		@Override
		public void reset() {
			index=0;
		}
		
	}

}
