package com.jin.pattern.iterator;

interface Iterator<T> {
	
	/**
	 * 如果仍有元素可以迭代，则返回 true
	 * @return
	 */
	public boolean hasNext();
	
	/**
	 * 返回迭代的下一个元素
	 * @return
	 */
	public T next();
	
	/**
	 * 将迭代器索引重置
	 */
	public void reset();

}
