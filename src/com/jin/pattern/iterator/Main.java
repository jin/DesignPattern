package com.jin.pattern.iterator;

/**
 * 迭代子模式可以顺序地访问一个聚集中的元素而不必暴露聚集的内部表象
 * 例子简单地模拟了JDK集合框架的实现
 * @author Jin
 * @datetime 2015/1/27 下午5:50:07
 */
public class Main {

	public static void main(String[] args) {
		String[] strArr = {"hello","world","Java","design","pattern"};
		Collection<String>collection = new ConcreteCollection<>(strArr);
		Iterator<String> iterator = collection.iterator();
		while(iterator.hasNext()){
			System.out.println(iterator.next());
		}
		iterator.reset();
	}
}
