package com.jin.pattern.mediator;

/**
 * 同事类A
 * @author Jin
 * @datetime 2015年1月31日 下午11:41:26
 */
public class ConcreteColleagueA extends Colleague {

	private String msg;
	
	public ConcreteColleagueA(Mediator mediator) {
		super(mediator);
	}

	public void doSomething(){
		setMsg("Hello world!");
		System.out.println("ColleagueA has changed!");
		change();
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}
	
}
