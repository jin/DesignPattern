package com.jin.pattern.mediator;

/**
 * 同事类B
 * @author Jin
 * @datetime 2015年1月31日 下午11:44:40
 */
public class ConcreteColleagueB extends Colleague {

	public ConcreteColleagueB(Mediator mediator) {
		super(mediator);
	}

	public void doSomething(String msg){
		System.out.println("Just received a message:"+msg);
		change();
	}
}
