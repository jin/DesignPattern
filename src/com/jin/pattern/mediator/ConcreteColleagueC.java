package com.jin.pattern.mediator;

/**
 * 同事类Ｃ
 * @author Jin
 * @datetime 2015年1月31日 下午11:53:48
 */
public class ConcreteColleagueC extends Colleague {

	public ConcreteColleagueC(Mediator mediator) {
		super(mediator);
	}

	public void doSomething(){
		System.out.println("Something have been changed!");
	}
}
