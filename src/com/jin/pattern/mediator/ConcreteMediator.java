package com.jin.pattern.mediator;

/**
 * 具体调停者
 * @author Jin
 * @datetime 2015年1月31日 下午11:57:00
 */
public class ConcreteMediator implements Mediator {

	private ConcreteColleagueA a;
	private ConcreteColleagueB b;
	private ConcreteColleagueC c;
	
	public ConcreteMediator() {
	}

	@Override
	public void changed(Colleague colleague) {
		if(colleague instanceof ConcreteColleagueA){
			getB().doSomething(((ConcreteColleagueA)colleague).getMsg());
		}else if(colleague instanceof ConcreteColleagueB){
			getC().doSomething();
		}
	}

	public ConcreteColleagueA getA() {
		return a;
	}

	public ConcreteColleagueB getB() {
		return b;
	}

	public ConcreteColleagueC getC() {
		return c;
	}

	public void setA(ConcreteColleagueA a) {
		this.a = a;
	}

	public void setB(ConcreteColleagueB b) {
		this.b = b;
	}

	public void setC(ConcreteColleagueC c) {
		this.c = c;
	}

}
