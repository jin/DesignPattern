package com.jin.pattern.mediator;

/**
 * 调停者模式
 * 包装了一系列对象相互作用的方式，使得这些对象不必相互明显引用，从而使它们可以较松散地耦合。
 * 当这些对象中的某些对象之间的相互作用发生改变时，不会立即影响到其他的一些对象之间的相互作用，
 * 从而保证这些相互作用可以彼此独立地变化。
 * @author Jin
 * @datetime 2015年2月1日 上午12:15:50
 */
public class Main {

	public static void main(String[] args) {
		ConcreteMediator  mediator = new ConcreteMediator();
		ConcreteColleagueA a = new ConcreteColleagueA(mediator);
		ConcreteColleagueB b = new ConcreteColleagueB(mediator);
		ConcreteColleagueC c = new ConcreteColleagueC(mediator);
		mediator.setA(a);
		mediator.setB(b);
		mediator.setC(c);
		a.doSomething();
	}
}
