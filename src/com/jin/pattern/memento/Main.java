package com.jin.pattern.memento;

/**
 * 备忘录模式
 * 保存一个对象的某个状态，以便在适当的时候恢复对象
 * @author Jin
 * @datetime 2015/1/30 下午6:07:14
 */
public class Main {

	public static void main(String[] args) {
		Original original = new Original();
		original.setStatus("Panda");
		System.out.println("初始化状态："+original.toString());
		Storage storage = new Storage(original.createMemento());
		original.setStatus("Cat");
		System.out.println("修改后状态："+original.toString());
		original.restoreMemento(storage.getMemento());
		System.out.println("回复后状态："+original.toString());
		
		
	}
}
