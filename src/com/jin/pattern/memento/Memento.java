package com.jin.pattern.memento;

/**
 * 备忘录类
 * @author Jin
 * @datetime 2015/1/30 下午5:55:43
 */
public class Memento {

	private String value;

	public Memento(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}
	
}
