package com.jin.pattern.memento;

/**
 * 原始类
 * @author Jin
 * @datetime 2015/1/30 下午6:00:40
 */
public class Original {

	private String status;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	public Memento createMemento(){
		return new Memento(getStatus());
	}
	
	public void restoreMemento(Memento memento){
		setStatus(memento.getValue());
	}
	
	@Override
	public String toString() {
		return "[Status]->"+getStatus();
	}
}
