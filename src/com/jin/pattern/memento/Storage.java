package com.jin.pattern.memento;

/**
 * 备忘录储存类
 * @author Jin
 * @datetime 2015/1/30 下午5:57:33
 */
public class Storage {

	private Memento memento;

	public Storage(Memento memento) {
		this.memento = memento;
	}

	public Memento getMemento() {
		return memento;
	}
}
