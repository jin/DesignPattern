package com.jin.pattern.observer;

/**
 * 观察者
 * 实际上Java JDK中已经给出了观察者模式的实现，参见java.util.Observable（被观察者）和java.util.Observer（观察者接口）
 * @author Jin
 * @datetime 2015/1/27 下午2:08:35
 */
public class ConcreteObserver implements Observer {

	private Subject subject;
	public ConcreteObserver(Subject subject) {
		this.subject = subject;
	}

	@Override
	public void update() {
		System.out.println("Receive content:"+subject.getContent());
	}
	
}
