package com.jin.pattern.observer;

import java.util.ArrayList;
import java.util.List;

/**
 * 被观察者
 * @author Jin
 * @datetime 2015/1/27 下午2:09:29
 */
public class ConcreteSubjec implements Subject {

	private List<Observer> observers = new ArrayList<Observer>();
	
	@Override
	public void addObserver(Observer observer) {
		observers.add(observer);
	}

	@Override
	public void notification() {
		for (Observer o : observers) {
			o.update();
		}
	}

	@Override
	public void removeObserver(Observer observer) {
		observers.remove(observer);
	}

	@Override
	public void operation() {
		this.content = "Something changed!";
		notification();
	}
	
	private String content;
	
	@Override
	public String getContent(){
		return this.content;
	}
	
}
