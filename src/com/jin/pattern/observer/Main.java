package com.jin.pattern.observer;

public class Main {

	public static void main(String[] args) {
		Subject subject = new ConcreteSubjec();
		subject.addObserver(new ConcreteObserver(subject));
		subject.addObserver(new ConcreteObserver(subject));
		subject.addObserver(new ConcreteObserver(subject));
		
		subject.operation();
	}
}
