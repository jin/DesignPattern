package com.jin.pattern.observer;

interface Observer {
	/**
	 * 接受通知后的操作
	 */
	public void update();
}
