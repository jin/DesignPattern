package com.jin.pattern.observer;

interface Subject {

	/**
	 * 新增观察者
	 * @param observer
	 */
	public void addObserver(Observer observer);
	
	/**
	 * 删除观察者
	 * @param observer
	 */
	public void removeObserver(Observer observer);
	
	/**
	 * 通知所有的观察者
	 */
	public void notification();
	
	/**
	 * 自身操作
	 */
	public void operation();
	
	/**
	 * 获取变动内容
	 * @return
	 */
	public String getContent();
}
