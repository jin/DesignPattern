package com.jin.pattern.prototype;

public class Main {

	public static void main(String[] args) throws Exception {
		Rabbit rabbit = new Rabbit();
		rabbit.setColor("White");
		Prototype prototype = new Prototype(1, "Jack", rabbit);
		System.out.println(prototype.toString());
		Prototype prototype1 = prototype.shallowClone();
		Prototype prototype2 = prototype.deepClone1();
		Prototype prototype3 = prototype.deepClone2();
		rabbit.setColor("Red");
		System.out.println(prototype1.toString());
		System.out.println(prototype2.toString());
		System.out.println(prototype3.toString());
	}
	
}
