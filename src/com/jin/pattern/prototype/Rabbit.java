package com.jin.pattern.prototype;

import java.io.Serializable;

public class Rabbit implements Cloneable, Serializable {
	private static final long serialVersionUID = 7703196275029380505L;
	private String color;

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}
	
	public Rabbit clone(){
		Rabbit rabbit = null;
		try {
			rabbit = (Rabbit) super.clone();
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}
		return rabbit;
	}
	
}
