package com.jin.pattern.proxy;

import com.jin.pattern.decorator.Component;

public class Main {

	public static void main(String[] args) {
		Component component = new Proxy();
		component.operation();
	}
	
}
