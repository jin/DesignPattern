package com.jin.pattern.proxy;

import com.jin.pattern.decorator.Component;
import com.jin.pattern.decorator.ConcreteComponent;

/**
 * 代理模式
 * 代理模式与装饰器模式非常像似，但代理模式控制了被代理对象的访问权限，决定其执行与否，
 * 而装饰模式对被装饰对象没有控制权，只是增强其操作
 * @author Jin
 * @datetime 2015/1/20 下午5:40:22
 */
public class Proxy implements Component {

	private Component component;
	
	public Proxy() {
		this.component = new ConcreteComponent();
	}


	@Override
	public void operation() {
		System.out.println("Do before!");
		component.operation();
		System.out.println("Do after!");
	}

}
