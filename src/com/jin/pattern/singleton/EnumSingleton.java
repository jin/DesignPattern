package com.jin.pattern.singleton;

/**
 * 枚举单例
 * 默认枚举实例的创建是线程安全的，并且JVM对枚举单例的序列化有保证。
 * @author Jin
 * @datetime 2015年2月1日 下午9:29:54
 */
public enum EnumSingleton {

	INSTANCE;
	
	public void doSomething(){
		System.out.println("Hello world!");
	}
	
}
