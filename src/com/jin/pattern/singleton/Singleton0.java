package com.jin.pattern.singleton;

import java.io.Serializable;

/**
 * 单例模式(Singleton)
 * 饿汉式，空间换时间，线程安全，利用jvm的类装载机制，确保只有一个实例
 * 对于Singleton0~Singleton3这些传统单例实现，如果要实现序列化接口，必须要定义readResolve方法来确保单例
 * @author Jin
 * @datetime 2015/1/10 下午2:38:48
 */
public class Singleton0 implements Serializable {
	private static final long serialVersionUID = -1375815522671529742L;
	private static Singleton0 INSTANCE = new Singleton0();

	private Singleton0(){
	}
	
	public static Singleton0 getInstance(){
		return INSTANCE;
	}
	
	private Object readResolve(){
        return INSTANCE;
    }
}
