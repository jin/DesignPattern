package com.jin.pattern.state;

/**
 * 具体状态角色A
 * @author Jin
 * @datetime 2015年1月30日 下午7:56:36
 */
public class ConcreteStateA implements State {

	@Override
	public void handle(Context context) {
		System.out.println("State->A");
		context.setState(new ConcreteStateB());
	}

}
