package com.jin.pattern.state;

/**
 * 具体状态角色B
 * @author Jin
 * @datetime 2015年1月30日 下午7:57:18
 */
public class ConcreteStateB implements State {

	@Override
	public void handle(Context context) {
		System.out.println("State->B");
		context.setState(new ConcreteStateA());
	}

}
