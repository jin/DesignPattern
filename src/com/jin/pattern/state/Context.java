package com.jin.pattern.state;

/**
 * 环境角色
 * @author Jin
 * @datetime 2015年1月30日 下午7:53:47
 */
public class Context {

	private State state;

	
	public Context() {
		this.state = new ConcreteStateA();
	}

	public void setState(State state) {
		this.state = state;
	}
	
	public void request(){
		state.handle(this);
	}
	
}
