package com.jin.pattern.state;

/**
 * 状态模式
 * 允许一个对象在其内部状态改变时改变它的行为
 * 关于状态模式，有很多不同的实现方法，常见的实现是把具体状态角色暴露给客户端，让客户端来决定当前状态，从而改变环境角色行为，
 * 仔细一看，发现这种实现与策略模式极其相似，而按照GOF的建议是不希望将状态角色暴露给客户程序的，与客户程序打交道的仅仅使用环境角色，
 * 客户是不知道系统是怎么实现的，更不关心什么有几个具体状态。至于状态的转换，建议是在具体状态角色中来指定后续状态以及何时转换
 * @author Jin
 * @datetime 2015年1月30日 下午8:01:49
 */
public class Main {

	public static void main(String[] args) {
		Context context = new Context();
		
		context.request();
		context.request();
		context.request();
		context.request();
	}
}
