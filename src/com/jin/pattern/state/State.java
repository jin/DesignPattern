package com.jin.pattern.state;

/**
 * 抽象状态角色
 * @author Jin
 * @datetime 2015年1月30日 下午7:39:22
 */
public interface State {

	public void handle(Context context);
}
