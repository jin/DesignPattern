package com.jin.pattern.strategy;

import java.math.BigDecimal;

/**
 * 打八折
 * @author Jin
 * @datetime 2015/1/23 下午3:07:26
 */
public class Discount1 implements Settlement {

	@Override
	public double finalPrice(double bill) {
		return BigDecimal.valueOf(bill * 0.8).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
	}

}
