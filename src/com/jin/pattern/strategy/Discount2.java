package com.jin.pattern.strategy;

/**
 * 满200减30
 * @author Jin
 * @datetime 2015/1/23 下午3:16:48
 */
public class Discount2 implements Settlement {

	@Override
	public double finalPrice(double bill) {
		if(bill >= 200){
			return bill - 30;
		}else{
			return bill;
		}
	}

}
