package com.jin.pattern.strategy;

/**
 * 策略模式
 * 系统定义一系列算法，并将每个算法封装起来，且算法之间可以相互替换，算法的变化并不会影响到客户端调用
 * 策略模式多用在算法决策系统中，外部用户只需要决定用哪个算法即可
 * @author Jin
 * @datetime 2015/1/23 下午3:24:32
 */
public class Main {

	public static void main(String[] args) {
		double bill = 200;
		Settlement discount1 = new Discount1();
		Settlement discount2 = new Discount2();
		double d1 = discount1.finalPrice(bill);
		double d2 = discount2.finalPrice(bill);
		System.out.println("账单总价："+bill);
		System.out.println("折扣一付款："+d1);
		System.out.println("折扣二付款："+d2);
	}
}
