package com.jin.pattern.strategy;

/**
 * 结算系统
 * @author Jin
 * @datetime 2015/1/23 下午3:02:26
 */
public interface Settlement {

	/**
	 * 折扣算法
	 * @param bill
	 * @return 实际付款
	 */
	public double finalPrice(double bill);
	
}
