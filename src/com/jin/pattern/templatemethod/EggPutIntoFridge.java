package com.jin.pattern.templatemethod;

public class EggPutIntoFridge extends PutIntoFridge {

	@Override
	protected void how() {
		System.out.println("一个一个拾起鸡蛋，并将鸡蛋放进保鲜室！");
	}

}
