package com.jin.pattern.templatemethod;


public class ElephantPuIntoFridge extends PutIntoFridge {

	@Override
	protected void how() {
		System.out.println("将大象大卸八块，并将每一块放进冷藏室！");
	}

}
