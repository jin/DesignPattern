package com.jin.pattern.templatemethod;

/**
 * 模板方法模式
 * 由抽象类中模板方法定义顶级逻辑，将部分具体逻辑交给子类实现
 * 最常用的设计模式之一，在各种框架中可以经常看见其身影
 * @author Jin
 * @datetime 2015/1/23 下午3:56:38
 */
public class Main {

	public static void main(String[] args) {
		PutIntoFridge p1 = new EggPutIntoFridge();
		PutIntoFridge p2 = new ElephantPuIntoFridge();
		p1.put();
		p2.put();
	}
}
