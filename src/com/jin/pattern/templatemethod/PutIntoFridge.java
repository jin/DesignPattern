package com.jin.pattern.templatemethod;

public abstract class PutIntoFridge {

	public void put(){
		open();
		how();
		close();
	}
	
	private void open(){
		System.out.println("打开冰箱门。");
	}
	
	private void close(){
		System.out.println("关上冰箱门。");
	}
	
	protected abstract void how();
}
