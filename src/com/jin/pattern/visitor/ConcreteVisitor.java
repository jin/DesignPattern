package com.jin.pattern.visitor;

/**
 * 访问者
 * @author Jin
 * @datetime 2015年1月30日 下午8:48:46
 */
public class ConcreteVisitor implements Visitor {

	@Override
	public void visit(ElementA element) {
		element.operationA();
	}

	@Override
	public void visit(ElementB element) {
		element.operationB();
	}

}
