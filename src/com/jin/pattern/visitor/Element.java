package com.jin.pattern.visitor;

/**
 * 抽象元素类
 * @author Jin
 * @datetime 2015年1月30日 下午8:46:31
 */
public interface Element {

	public void accept(Visitor visitor);
}
