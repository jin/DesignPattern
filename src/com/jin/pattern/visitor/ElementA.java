package com.jin.pattern.visitor;

/**
 * 元素A
 * @author Jin
 * @datetime 2015年1月30日 下午8:51:15
 */
public class ElementA implements Element {

	@Override
	public void accept(Visitor visitor) {
		visitor.visit(this);
	}
	
	public void operationA(){
		System.out.println("operation from A");
	}

}
