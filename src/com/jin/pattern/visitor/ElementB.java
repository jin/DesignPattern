package com.jin.pattern.visitor;

/**
 * 元素B
 * @author Jin
 * @datetime 2015年1月30日 下午8:54:40
 */
public class ElementB implements Element {

	@Override
	public void accept(Visitor visitor) {
		visitor.visit(this);
	}

	public void operationB(){
		System.out.println("operation from B");
	}
}
