package com.jin.pattern.visitor;

/**
 * 访问者模式
 * 封装一些施加于某种数据结构元素之上的操作,一旦这些操作需要修改的话，接受这个操作的数据结构则可以保持不变。
 * 数据结构的每一个元素都可以接受一个访问者的调用，此元素向访问者对象传入元素对象，而访问者对象则返过来执行元素对象的操作。
 * @author Jin
 * @datetime 2015年1月30日 下午9:06:49
 */
public class Main {

	public static void main(String[] args) {
		ObjectStructure objectStructure = new ObjectStructure();
		objectStructure.add(new ElementA());
		objectStructure.add(new ElementB());
		Visitor visitor = new ConcreteVisitor();
		objectStructure.action(visitor);
	}
}
