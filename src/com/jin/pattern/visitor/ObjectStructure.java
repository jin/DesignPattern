package com.jin.pattern.visitor;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * 数据结构对象
 * 包含一个容纳多个不同类、不同接口的容器
 * @author Jin
 * @datetime 2015年1月30日 下午9:00:24
 */
public class ObjectStructure {

	private List<Element> elements = new ArrayList<>();
	
	public void add(Element element) {
		elements.add(element);
	}
	
	/**
	 * 统一访问每一个元素
	 * @param visitor 访问者
	 */
	public void action(Visitor visitor){
		Iterator<Element>iterator = elements.iterator();
		while(iterator.hasNext()){
			iterator.next().accept(visitor);
		}
	}
}
