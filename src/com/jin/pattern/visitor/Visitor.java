package com.jin.pattern.visitor;

/**
 * 抽象访问者
 * @author Jin
 * @datetime 2015年1月30日 下午8:45:17
 */
public interface Visitor {

	public void visit(ElementA element);
	
	public void visit(ElementB element);
}
